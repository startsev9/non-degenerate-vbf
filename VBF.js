const log = console.log;

function fisherYatesShuffle(arr) {
	const n = arr.length;
	for (let i = n - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[arr[i], arr[j]] = [arr[j], arr[i]];
	}
}

function generateRandomPermutation(n) {
	const permutation = Array.from({ length: 2 ** n }, (_, index) => index);
	fisherYatesShuffle(permutation);
	return permutation;
}

function mobiusTransformation(array, n) {
    mobius = [];
	for (let i = 0; i < n; i++) {
		for (let j = 0; j < Math.pow(2, n); j++) {
			if ((j & (1 << i)) !== 0) {
				mobius[j] = array[j] ^ array[j - (1 << i)];
			}
		}
	}
	return mobius
}

function getV(mobuisVector, numberOfVariables) {
	let v = '';
	const vecLenght = 1 << numberOfVariables;
	for (let i = 0; i < numberOfVariables; i++) {
		let c = 0;
		for (let a = 0; a < vecLenght; a++) {
			if ((mobuisVector[a] >> i) & 1) {
				c |= a;
			}
		}
		c === vecLenght - 1 ? v += '0' : v += '1';
	}
	return parseInt(v, 2);
}

function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function swapXnY (V, permutation) {
	const x = getRandomInt(0, permutation.length - 1);
	const Fx = permutation[x];
	const Fy = Fx ^ V;
	for (let i = 0; i < permutation.length; i++) {
		if (x != i && permutation[i] === Fy) {
			[permutation[i], permutation[x]] = [permutation[x], permutation[i]];
			return permutation;
		}
	}
}

function generateNonDegenaratePermutation (numberOfVariables) {
	let arr = generateRandomPermutation(numberOfVariables);
    //log(arr);
	const mobius = mobiusTransformation(arr, numberOfVariables);
	const v = getV (mobius, numberOfVariables);
	if (v === 0){
		return 0;
	}
	const nondegeneratepermutation = swapXnY (v, arr);
	//log ('V =', v,'\nБыла произведена перестановка x и y\n', nondegeneratepermutation);
	return 1;
}

console.time('\x1b[32mВремя выполнения\x1b[0m');
const numberOfVariables = 26;

let degenerateCounter = 0;
let nondegenerateCounter = 0;
log('\x1b[36mКоличество переменных:\x1b[0m', numberOfVariables);

let interval = 0;
/*
for (interval = 0; interval < 1_000_000_000; interval++) {
	generateNonDegenaratePermutation(numberOfVariables) ? degenerateCounter++ : nondegenerateCounter++;
	if (interval % 1_000_000 === 0) {
		log('\x1b[36mi =\x1b[0m', interval, '\nФункций без фиктивной переменной:', nondegenerateCounter, '\nФункций с фиктивной переменной:', degenerateCounter);
	}
}
log('Функций без фиктивной переменной:', nondegenerateCounter, '\nФункций с фиктивной переменной:', degenerateCounter);
*/
generateNonDegenaratePermutation(numberOfVariables);

console.timeEnd('\x1b[32mВремя выполнения\x1b[0m');
