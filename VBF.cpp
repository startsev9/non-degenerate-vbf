#include <vector>
#include <algorithm>
#include <random>
#include <cmath>
#include <iostream>
#include <chrono>
#include <numeric>
#include <string>

void fisherYatesShuffle(std::vector<int>& arr) {
    int n = arr.size();
    std::random_device rd;
    std::mt19937 gen(rd());
    for (int i = n - 1; i > 0; i--) {
        std::uniform_int_distribution<> dis(0, i + 1);
        int j = dis(gen);
        std::swap(arr[i], arr[j]);
    }
}

std::vector<int> generateRandomPermutation(int n) {
    std::vector<int> permutation(1 << n);
    std::iota(permutation.begin(), permutation.end(), 0);
    fisherYatesShuffle(permutation);
    return permutation;
}

std::vector<int> mobiusTransformation(std::vector<int> array, int n) {
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < 1 << n; j++) {
            if ((j & (1 << i)) != 0) {
                array[j] ^= array[j - (1 << i)];
            }
        }
    }
    return array;
}

int getV(std::vector<int> mobiusVector, int numberOfVariables) {
    int v = 0;
    int vecLength = 1 << numberOfVariables;
    for (int i = 0; i < numberOfVariables; i++) {
        int c = 0;
        for (int a = 0; a < vecLength; a++) {
            if ((mobiusVector[a] >> i) & 1) {
                c |= a;
            }
        }
        v |= (c == vecLength - 1) ? 0 : 1 << i;
    }
    return v;
}

int getRandomInt(int min, int max) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(min, max);
    return dis(gen);
}

void swapXnY(int V, std::vector<int>& permutation) {
    int x = getRandomInt(0, permutation.size() - 1);
    int Fx = permutation[x];
    int Fy = Fx ^ V;
    for (int i = 0; i < permutation.size(); i++) {
        if (x != i && permutation[i] == Fy) {
            std::swap(permutation[i], permutation[x]);
            return;
        }
    }
}

int generateNonDegeneratePermutation(int numberOfVariables) {
    std::vector<int> arr = generateRandomPermutation(numberOfVariables);
    std::vector<int> mobius = mobiusTransformation(arr, numberOfVariables);
    int v = getV(mobius, numberOfVariables);
    /*
    for(int i = 0; i < arr.size(); i++) {
        std::cout << arr[i] << ' ';
    }
    std::cout << '\n' << v << '\n' << std::endl;
    
    for(int i = 0; i < mobius.size(); i++) {
        std::cout << mobius[i] << ' ';
    }
    */

    if (v == 0){
        return 0;
    }
    //swapXnY(v, arr);
    return 1;
}

int main() {
    setlocale(LC_ALL, "");

    auto start = std::chrono::high_resolution_clock::now();
    int numberOfVariables = 5;
    std::wcout << L"Количество переменных: " << numberOfVariables << std::endl;
    int degenerateCounter = 0;
    int nondegenerateCounter = 0;

    for (int interval = 0; interval < 1000000000; interval++) {
        generateNonDegeneratePermutation(numberOfVariables) ? degenerateCounter++ : nondegenerateCounter++;
    }
    std::wcout << L"Функций без фиктивной переменной: " << nondegenerateCounter
            << L"\nФункций с фиктивной переменной: " << degenerateCounter << std::endl;

    //generateNonDegeneratePermutation(numberOfVariables);
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Execution time: " << elapsed.count() << " seconds\n";
    return 0;
}
