const log = console.log;

class BoolFunction {
	#numberOfVars;
	#arrayLength;
	#array = new Uint32Array();

	constructor () {
		if (arguments.length === 1 && typeof arguments[0] === 'string') {
			let string = arguments[0];
			let stringLength = string.length;
			
			if (stringLength != 0 && ((stringLength & (stringLength - 1)) === 0)) {
				this.#numberOfVars = 0;
				for (let i = 1; i != stringLength; i <<= 1, this.#numberOfVars++) { }
				
				if (stringLength < 32) {
					this.#arrayLength = 1;
					this.#array = new Uint32Array(1);
					(string[stringLength - 1] == '1') ? this.#array[0] = 1 : this.#array[0] = 0;

					for (let i = stringLength - 2; i >= 0; i--) {
						this.#array[0] <<= 1;
						if (string[i] === '1') this.#array[0] |= 1;
					}
				}

				else {
					this.#arrayLength = stringLength >>> 5;
					this.#array = new Uint32Array(this.#arrayLength);
					for (let i = 0; i < this.#arrayLength; ++i) {
						string[32 * i + 31] === '1' ? this.#array[i] = 1 : this.#array[i] = 0;

						for (let j = 30; j >= 0; j--) {
							this.#array[i] <<= 1;
							if (string[32 * i + j] === '1') {
								this.#array[i] |= 1;
							}
						}
					}
				}
			}

			else {
				this.#numberOfVars = 0;
				this.#arrayLength = 0;
				this.#array = new Uint32Array();
				throw new TypeError('Введена неверная строка, объект будет создан с нулевыми значениями');
			}
		}
		else if (arguments.length === 2 && 
				typeof arguments[0] === 'number' && 
				arguments[0] >= 1 && 
				typeof arguments[1] === 'number' && 
				[0, 1, 2].includes(arguments[1])) {
			
			this.#numberOfVars = arguments[0];

			if (arguments[1] === 0) {
				this.#numberOfVars < 5 ? this.#arrayLength = 1 : this.#arrayLength = 1 << this.#numberOfVars - 5;
				this.#array = new Uint32Array(this.#arrayLength);
				for (let i = 0; i < this.#arrayLength; ++i) { this.#array[i] = 0; }
			}

			else if (arguments[1] === 1) {
				if (this.#numberOfVars < 5) {
					this.#arrayLength = 1
					this.#array = new Uint32Array(this.#arrayLength);
					this.#array[0] = (1 << (1 << this.#numberOfVars)) - 1;
				} 
				else {
					this.#arrayLength = 1 << (this.#numberOfVars - 5);
					this.#array = new Uint32Array(this.#arrayLength);
					for (let i = 0; i < this.#arrayLength; ++i) {
						this.#array[i] = 4294967295;
					}
				}
			}

			else if (arguments[1] === 2) {
				if (this.#numberOfVars < 5) {
					this.#arrayLength = 1;
					this.#array = new Uint32Array(this.#arrayLength);
					let max = 1 << (1 << this.#numberOfVars);
					this.#array[0] = getRandomInt(0, max);
				}
				else {
					this.#arrayLength = 1 << (this.#numberOfVars - 5);
					this.#array = new Uint32Array(this.#arrayLength);
					for (let i = 0; i < this.#arrayLength; ++i) {
						this.#array[i] = getRandomInt(0, 4294967296);
					}
				}
			}
		}
	}

	get array() {
		return this.#array;
	}

	get numberOfVars() {
		return this.#numberOfVars;
	}

	get BoolFunc() {
		const binaryArray = new Array(this.#arrayLength * 32).fill(0);
	
		for (let i = 0; i < this.#arrayLength; ++i) {
			let num = this.#array[i];
			for (let j = 0; j < 32; j++) {
				binaryArray[(i * 32) + j] = (num & 1);
				num >>= 1;
			}
		}
		return binaryArray.join('');
	}

	add(other) {
		if (this.#numberOfVars !== other.#numberOfVars) {
			throw new Error('Невозможно сложить функции с разным количеством переменных');
		}

		let result = new BoolFunction(this.#numberOfVars, 0);
		for (let i = 0; i < this.#arrayLength; ++i) {
			result.#array[i] = this.#array[i] ^ other.#array[i];
		}

		return result;
	}
	
	mobiusTransformation() {
		this.#array = this.#array.map(num => {
			num ^= (num << 1) & 0xAAAAAAAA;
			num ^= (num << 2) & 0xCCCCCCCC;
			num ^= (num << 4) & 0xF0F0F0F0;
			num ^= (num << 8) & 0xFF00FF00;
			num ^= (num << 16) & 0xFFFF0000;
			return this.#numberOfVars < 5 ? num & ((1 << (1 << this.#numberOfVars)) - 1) : num;
		});

		let stepCount = this.#numberOfVars - 5;
		let sumCount = this.#arrayLength >>> 1;
		let count = 1;
		let index = 0;

		for (let i = 0; i < stepCount; ++i) {
			let steps = 1 << i;
			index = steps;
			for (let j = 0; j < sumCount; ++j) {
				this.#array[index] ^= this.#array[index - steps];
				count === steps ? (count = 1, index += steps + 1) : (++count, ++index);
			}
		}
		return this;
	}

	transform() {
		let copy = new BoolFunction(this.#numberOfVars, 0);
		copy.#array = Uint32Array.from(this.#array);

		return copy.mobiusTransformation();
	}

	degree () {
		let copy = this.transform();
		let count = 0, degree = 0, maxDegree = 0, tmp;
		for (let i = 0, j = 0; i < copy.#arrayLength; ++i, j = 0) {
			while (j < 32) {
				if ((1 << j) & copy.#array[i]) {
					tmp = count;
					while (tmp != 0) {
						tmp &= (tmp - 1);
						++degree;
					}
					maxDegree = degree > maxDegree ? degree: maxDegree;
					degree = 0;
				}
				++count;
				++j;
			}
		}
		return maxDegree;
	}

	WalshHadamard() {
		let j = 0, k = 0;
		let numberOfValues = 1 << this.#numberOfVars;
		let count = this.#numberOfVars < 5 ? 1 << this.#numberOfVars : 32; 
		let F = new Array(numberOfValues);

		for (let i = 0; i < this.#arrayLength; ++i) {
			for (j = 0; j < count; ++j, ++k) {
				F[k] = 1 << j & this.#array[i] ? -1 : 1;
			}
		}

		let n = 1, m;
		let G = new Array(numberOfValues / 2);
		for (let i = 0; i < this.#numberOfVars; ++i, n *= 2) {
			for (let j = 0; j < numberOfValues / (n << 1); ++j) {
				m = n + 2 * n * j;
				k = 2 * n * j;
				for (let t = 0, tempK = k, tempM = m; t < n; ++t, ++tempM, ++tempK) {
					G[t] = F[tempK];
					F[tempK] += F[tempM];
				}
				for (let t = 0; t < n; ++t, ++m)	F[m] = G[t] - F[m];
			}
		}
		
		return F;
	}
}


class VectorBoolFunction {
	#functions = [];

	constructor(inputStrings) {
		this.#functions = inputStrings.map(inputStr => new BoolFunction(inputStr));
	}

	VectorMobiusTransformation() {
		this.#functions.forEach(boolFunction => boolFunction.mobiusTransformation());
	}

	getFunctions() {
		return this.#functions;
	}

	getVectors() {
		return this.#functions.reduce((vectors, boolFunction) => {
			vectors.push(boolFunction.BoolFunc);
			return vectors;
		}, []);
	}
}
  

function fisherYatesShuffle(arr) {
	const n = arr.length;
	for (let i = n - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[arr[i], arr[j]] = [arr[j], arr[i]];
	}
}

function generateRandomPermutation(n) {
	const permutation = Array.from({ length: 1 << n }, (_, index) => index);
	fisherYatesShuffle(permutation);
	return permutation;
}

function toBinaryVector(num, n) {
	const binaryArray = new Array(n);
	for (let i = n - 1; i >= 0; i--) {
		binaryArray[i] = num & 1;
		num >>= 1;
	}
	return binaryArray;
}

function isArraysEqual(firstArray, secondArray) {
	return firstArray.toString() === secondArray.toString();
}

function getRandomVBFPermutation(randomPermutation, numberOfVariables) {
	const binaryVectors = randomPermutation.map(num => toBinaryVector(num, numberOfVariables));

	const inputStrings = Array.from({ length: numberOfVariables }, (_, i) =>
		binaryVectors.map(vector => vector[i]).join('')
	);

	return new VectorBoolFunction(inputStrings);
}

function analyzeVectorsForDegeneracy (randomPermutation, numberOfVariables) {
	//console.time('\x1b[34mПолучаем VBF\x1b[0m');
	let vectorBoolFunc = getRandomVBFPermutation(randomPermutation, numberOfVariables);
	//console.timeEnd('\x1b[34mПолучаем VBF\x1b[0m');

	//console.time('\x1b[35mПреобразование Мёбиуса\x1b[0m');
	vectorBoolFunc.VectorMobiusTransformation();
	//console.timeEnd('\x1b[35mПреобразование Мёбиуса\x1b[0m');

	//console.time('\x1b[35mМассив сформирован\x1b[0m');
	const mobuisVectorsStrings = vectorBoolFunc.getVectors();
	//console.timeEnd('\x1b[35mМассив сформирован\x1b[0m');


	//console.time('\x1b[31mПостроение v\x1b[0m');
	let v = '';
	const vecLength = 1 << numberOfVariables;
	for (let i = 0; i < numberOfVariables; i++) {
		let c = 0;
		for (let a = 0; a < vecLength; a++) {
			if (mobuisVectorsStrings[i].charAt(a) === '1') {
				c |= a;
			}
		}
		v |= (c == vecLength - 1) ? 0 : 1 << i;
	}
	//console.timeEnd('\x1b[31mПостроение v\x1b[0m');
	return v;
}

function getRandomInt(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function swapXnY (V, permutation) {
	const x = getRandomInt(0, permutation.length - 1);
	const Fx = permutation[x];
	const Fy = Fx ^ V;
	for (let i = 0; i < permutation.length; i++) {
		if (permutation[i] === Fy) {
			[permutation[i], permutation[x]] = [permutation[x], permutation[i]];
			return permutation;
		}
	}
}

function generateNonDegenaratePermutation (numberOfVariables) {
	const V = analyzeVectorsForDegeneracy(randomPermutation, numberOfVariables);
	//log('v =', V);
	if (V === 0){
		return 0;
	}
	//log(randomPermutation);
	//const nondegeneratepermutation = swapXnY (V, randomPermutation);
	//log ('v =', analyzeVectorsForDegeneracy(nondegeneratepermutation, numberOfVariables), '\n', nondegeneratepermutation);
	return 1;
}

function getDegenerateStatistics (numberOfVariables) {
	let degenerateCounter = 0;
	let nondegenerateCounter = 0;
	let interval = 0;
	
	for (interval = 0; interval < 1_000_000_000; interval++) {
		generateNonDegenaratePermutation(numberOfVariables) ? degenerateCounter++ : nondegenerateCounter++;
		if (interval % 1_000_000 === 0) {
			log('\x1b[36mi =\x1b[0m', interval, '\nФункций без фиктивной переменной:', nondegenerateCounter, '\nФункций с фиктивной переменной:', degenerateCounter);
		}
	}
	log('Функций без фиктивной переменной:', nondegenerateCounter, '\nФункций с фиктивной переменной:', degenerateCounter);
}

function getCharacteristics (randomPermutation, numberOfVariables) {
	let vectorBoolFunc = getRandomVBFPermutation(randomPermutation, numberOfVariables).getFunctions();
	let f = new BoolFunction(numberOfVariables, 0);
	let k;
	let W;
	let M;
	let d;
	let c = 0;
	let tmp = 0;
	let compdeg = numberOfVariables;
	let coorddeg = 0;
	let v = 0;
	let vecLength = 1 << numberOfVariables;
	
	for (let i = 1; i < vecLength; i++) {
		k = (((i - 1) ^ ((i - 1) >> 1)) ^ (i ^ (i >> 1)));
		f = f.add(vectorBoolFunc[Math.log2(k)]);
		/*Обработка f*/

		/*Вырожденность компонент*/
		if (v === 0) {
			M = f.transform().BoolFunc;
			let C = 0;
			for (let a = 0; a < vecLength; a++) {
				if (M[a] === '1') {
					C |= a;
				}
			}
			v = (C == vecLength - 1) ? 0 : 1 << i;
		}

		/*Нелинейность*/
		W = f.WalshHadamard();
		d = W.reduce((max, curr) => Math.max(max, Math.abs(curr)), 0);
		c = d > c ? d : c;
		/*Минимальная степень компонент*/
		tmp = f.degree();
		compdeg = tmp < compdeg ? tmp : compdeg;
		/*Максимальная степень компонент = степень векторной булевой функции*/
		coorddeg = tmp > coorddeg ? tmp : coorddeg;
	}
	
	v === 0 ? nondegenerateComponentsCounter++ : degenerateComponentsCounter++;

	let Nf = (1 << (numberOfVariables - 1)) - (c / 2);

	/*Анализ на наличие фиктивных переменных*/
	const V = analyzeVectorsForDegeneracy(randomPermutation, numberOfVariables);

	if (V === 0) {
		maxNonlinearityNonDegenerate = Nf > maxNonlinearityNonDegenerate ? Nf : maxNonlinearityNonDegenerate;
		nonlinearityNonDegenerateCounter += Nf;
		compdegNonDegCounter += compdeg;
		coorddegNonDegCounter += coorddeg;
		nondegenerateCounter++;
		return;
	}
	else {
		maxNonlinearityDegenerate = Nf > maxNonlinearityNonDegenerate ? Nf : maxNonlinearityDegenerate;
		nonlinearityDegenerateCounter += Nf;
		compdegDegCounter += compdeg;
		coorddegDegCounter += coorddeg;
		degenerateCounter++;
		return;
	}
}

function getNonlinearityStatistics (numberOfVariables) {
	for (interval = 0; interval < 100_000_000; interval++) {
		let randomPermutation = generateRandomPermutation(numberOfVariables);
		getCharacteristics(randomPermutation, numberOfVariables);
		/*
		if (interval % 1_000_000 === 0) {
			log('\x1b[36mi =\x1b[0m', interval);
			log('Функций без фиктивной переменной:', nondegenerateCounter, '\nФункций с фиктивной переменной:', degenerateCounter);
			log('Средняя нелинейность функций без фиктивной переменной:', nonlinearityNonDegenerateCounter / nondegenerateCounter, '\nСредняя нелинейность функций с фиктивной переменной:', nonlinearityDegenerateCounter / degenerateCounter);
			log('Максимальная степень координат:', coorddeg, '\nМинимальная степень компонент:', compdeg);
		}
		*/
	}
	log('Функций без фиктивной переменной:', nondegenerateCounter, '\nФункций с фиктивной переменной:', degenerateCounter);
	log('Средняя нелинейность функций без фиктивной переменной:', nonlinearityNonDegenerateCounter / nondegenerateCounter, '\nСредняя нелинейность функций с фиктивной переменной:', nonlinearityDegenerateCounter / degenerateCounter);
	log('Средняя максимальная степень координат без фиктивной переменной:', coorddegNonDegCounter / nondegenerateCounter, '\nСредняя минимальная степень компонент без фиктивной переменной:', compdegNonDegCounter / nondegenerateCounter);
	log('Средняя максимальная степень координат с фиктивной переменной:', coorddegDegCounter / degenerateCounter, '\nСредняя минимальная степень компонент с фиктивной переменной:', compdegDegCounter / degenerateCounter);
	log('Средняя максимальная степень координат:', (coorddegDegCounter + coorddegNonDegCounter) / interval, '\nСредняя минимальная степень компонент:', (compdegDegCounter + compdegNonDegCounter) / interval);
	log('Максимальная нелинейность функций без фиктивной переменной:', maxNonlinearityNonDegenerate, '\nМаксимальная нелинейность функций с фиктивной переменной:', maxNonlinearityDegenerate);
	log('Функций с невырожденными компонентами:', nondegenerateComponentsCounter, '\nФункций с вырожденными компонентами:', degenerateComponentsCounter);
}

// Main
console.time('\x1b[32mВремя выполнения\x1b[0m');
const numberOfVariables = 5;
var degenerateCounter = 0;
var nondegenerateCounter = 0;
var nonlinearityDegenerateCounter = 0;
var nonlinearityNonDegenerateCounter = 0;
var maxNonlinearityDegenerate = 0;
var maxNonlinearityNonDegenerate = 0;
var degenerateComponentsCounter = 0;
var nondegenerateComponentsCounter = 0;

var compdegNonDegCounter = 0;
var coorddegNonDegCounter = 0;
var compdegDegCounter = 0;
var coorddegDegCounter = 0;
var interval;

log('\x1b[36mКоличество переменных:\x1b[0m', numberOfVariables);

getNonlinearityStatistics(numberOfVariables);

//log(generateNonDegenaratePermutation(numberOfVariables));

console.timeEnd('\x1b[32mВремя выполнения\x1b[0m');
